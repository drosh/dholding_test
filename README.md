# dholding test

## Example ETL pipeline implementation

```buildoutcfg
$ pip install -r requirements.txt
$ python igc_pipeline.py -h
usage: igc_pipeline.py [-h] -i INPUT_FILE -o OUTPUT_FILE

IGC pipeline

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT_FILE, --input INPUT_FILE
                        IGC raw excel input file
  -o OUTPUT_FILE, --output OUTPUT_FILE
                        Normalized IGC csv output file
$ python igc_pipeline.py -i gmr520grainsSD.xlsx -o igc.csv
```

