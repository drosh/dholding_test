import pandas as pd

from abc import ABC, abstractmethod


class DataframeLoaderBase(ABC):

    @staticmethod
    @abstractmethod
    def load(df: pd.DataFrame, **kwargs):
        pass
