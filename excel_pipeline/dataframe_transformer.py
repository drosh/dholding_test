import pandas as pd

from abc import ABC, abstractmethod


class DataframeTransformerBase(ABC):

    @staticmethod
    @abstractmethod
    def transform(df: pd.DataFrame, **kwargs) -> pd.DataFrame:
        pass
