import openpyxl
import pandas as pd

from abc import ABC, abstractmethod


class ExcelExtractorBase(ABC):

    def __init__(self, input_file: str):
        self.workbook = openpyxl.load_workbook(input_file)
        self.validate()

    @abstractmethod
    def validate(self, *args, **kwargs):
        pass

    @abstractmethod
    def extract(self, *args, **kwargs) -> pd.DataFrame:
        pass
