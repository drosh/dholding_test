import argparse
from igc_pipeline import IGCPipeline


parser = argparse.ArgumentParser(description='IGC pipeline')
parser.add_argument(
    '-i', '--input', action="store", dest="input_file", help="IGC raw excel input file", required=True
)
parser.add_argument(
    '-o', '--output', action="store", dest="output_file", help="Normalized IGC csv output file", required=True
)

args = parser.parse_args()

IGCPipeline(args.input_file, args.output_file).etl()
