import logging

from .igc_extractor import IGCExtractor
from .igc_transformer import IGCTransformer
from .igc_loader import IGCLoader


class IGCPipeline(IGCExtractor, IGCTransformer, IGCLoader):

    def __init__(self, input_file: str, output_file: str):

        self.logger = logging.getLogger('IGCPipeline')
        self.logger.setLevel(logging.INFO)
        ch = logging.StreamHandler()
        formatter = logging.Formatter('%(asctime)s | %(name)s | %(levelname)s | %(message)s')
        ch.setFormatter(formatter)
        self.logger.addHandler(ch)

        self.input_file = input_file
        self.output_file = output_file
        IGCExtractor.__init__(self, input_file)

    def etl(self):
        self.logger.info(f'Extracting data from {self.input_file}')
        df = self.extract()

        self.logger.info(f'Transforming data with shape {df.shape}')
        df = self.transform(df)

        self.logger.info(f'Loading result data to {self.output_file}')
        self.load(df, self.output_file)

        self.logger.info('Complete')
