from openpyxl.utils import get_column_interval
import pandas as pd

from excel_pipeline.excel_extractor import ExcelExtractorBase


class IGCExtractor(ExcelExtractorBase):

    left_border = 'B'
    right_border = 'Q'
    header_size = 2
    column_names = [
        'season',
        'estimate type',
        'Opening Stocks',
        'Production',
        'Imports',
        'Total Supply',
        'Food Use',
        'Industrial use',
        'Feed Use',
        'Total Use',
        'Exports',
        'Closing Stocks'
    ]

    def validate(self):
        """
        :raises ValueError if validation failed
        """
        # TODO: validate input file has valid structure
        pass

    def extract(self) -> pd.DataFrame:
        """
        Parse IGC excel file into dataframe
        :return: dataframe with igc raw data
        """
        sheet = self.workbook.active

        data_columns = get_column_interval(self.left_border, self.right_border)

        df = pd.DataFrame(columns=self.column_names + ['commodity', 'country_region', 'element_unit'])

        current_commodity = None
        current_top = None
        header_font_color = None
        current_country_region = None
        current_element_unit = None
        # Iterate over all rows to get data from bordered tables
        for row in range(1, sheet.max_row):
            left_cell = sheet[f"{self.left_border}{row}"]
            right_cell = sheet[f"{self.right_border}{row}"]

            # Check if current row inside table
            is_table = left_cell.border.left.color == right_cell.border.right.color is not None
            if not is_table:
                # Clear all temporary values if we are outside of the table
                current_commodity = None
                current_top = None
                header_font_color = None
                current_country_region = None
                current_element_unit = None
                continue

            # Check border to find if we are in the table top row
            is_top = left_cell.border.top.color == right_cell.border.top.color == left_cell.border.left.color

            if is_top:
                current_top = row
                header_font_color = left_cell.font.color
                current_element_unit = sheet[f"{self.right_border}{row - 1}"].value
                current_country_region = None
                continue

            # Skip table header
            is_header = row - current_top < self.header_size
            if is_header:
                continue

            # Table commodity value has the same bg color as header
            if current_commodity is None:
                is_commodity = left_cell.font.color == header_font_color and left_cell.value
                if is_commodity:
                    current_commodity = left_cell.value
                    continue

            # Region has bold font
            is_country_region = left_cell.font.b is True
            if is_country_region:
                current_country_region = left_cell.value
                continue

            # There are some empty columns between payload cells, we are going to ignore them
            data_list = [sheet[f"{col}{row}"].value for col in data_columns if sheet[f"{col}{row}"].value is not None]

            # Our payload row should have all required columns
            if len(data_list) != len(self.column_names):
                continue

            data = dict(zip(self.column_names, data_list))
            data['commodity'] = current_commodity
            data['country_region'] = current_country_region
            data['element_unit'] = current_element_unit

            df = df.append(pd.Series(data=data), ignore_index=True)

        return df
