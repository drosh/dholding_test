import pandas as pd

from excel_pipeline.dataframe_loader import DataframeLoaderBase


class IGCLoader(DataframeLoaderBase):

    @staticmethod
    def load(df: pd.DataFrame, filename: str):
        """
        :param df: dataframe to load
        :param filename: output csv filename
        """
        df.to_csv(filename, index=False)
