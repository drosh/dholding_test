import pandas as pd
import re
import datetime as dt

from excel_pipeline.dataframe_transformer import DataframeTransformerBase


class IGCTransformer(DataframeTransformerBase):

    variable_columns = [
        'Opening Stocks',
        'Production',
        'Imports',
        'Total Supply',
        'Food Use',
        'Industrial use',
        'Feed Use',
        'Total Use',
        'Exports',
        'Closing Stocks'
    ]

    @staticmethod
    def transform(df: pd.DataFrame, **kwargs) -> pd.DataFrame:
        """
        Raw IGC data normalization
        :param df: raw IGC dataframe
        :return: normalized dataframe
        """
        pd.set_option('mode.chained_assignment', None)

        now = dt.datetime.utcnow()
        valid_from = now.strftime('%Y-%m-%d')
        valid_to = (now + dt.timedelta(days=30)).strftime('%Y-%m-%d')

        def extract_country_period(s):
            match = re.match(r'(.+) \((.+\/.+)\)', s)
            if not match:
                return s, pd.NA
            return match.group(1), match.group(2)

        df['marketing_period'] = df.apply(
            lambda row: extract_country_period(
                row['country_region']
            )[1],
            axis=1
        )

        df['country_region'] = df.apply(
            lambda row: extract_country_period(
                row['country_region']
            )[0],
            axis=1
        )

        df = df.dropna(subset=['marketing_period'])

        df['source'] = 'IGC'
        df['valid_from'] = valid_from
        df['valid_to'] = valid_to

        id_vars = [col for col in df.columns if col not in IGCTransformer.variable_columns]
        df = pd.melt(
            df, id_vars=id_vars, value_vars=IGCTransformer.variable_columns, var_name='element', value_name='vl_value'
        )

        df = df[[
            "source",
            "country_region",
            "commodity","element",
            "season",
            "vl_value",
            "element_unit",
            "valid_from",
            "valid_to",
            "estimate type",
            "marketing_period"
        ]]

        return df
